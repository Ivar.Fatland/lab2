package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    private final int capacity = 20;
    private List<FridgeItem> items = new ArrayList<>(capacity);

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() < totalSize())
            return items.add(item);
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) throws NoSuchElementException{
        if(!items.contains(item))
            throw new NoSuchElementException();
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired_items = items.stream()
            .filter(item -> item.hasExpired())
            .toList();
        items.removeAll(expired_items);
        return expired_items;
    }

}
